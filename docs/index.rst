.. SemanticLayerTools documentation master file, created by
   sphinx-quickstart on Fri Sep 24 14:43:14 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SciCom documentation
====================

Simulating various aspects of scientific communication via Agent-based models.

The development is part of the research project `ModelSEN <https://modelsen.mpiwg-berlin.mpg.de>`_
Socio-epistemic networks: Modelling Historical Knowledge Processes,
part of Department I of the Max Planck Institute for the History of Science and
funded by the Federal Ministry of Education and Research, Germany (Grant No. 01 UG2131).

.. image:: _static/bmbf.png

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   readme
   usingmesa
   letters
   authors
   license


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
