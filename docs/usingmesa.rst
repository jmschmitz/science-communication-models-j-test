Running the models
==================

All models are based on the mesa framework for Agent-based modelling in Python. 

Models can be used as part of a batch script or in a Jupyter Notebook. For an example, see the examples folder. 

In addition, the 'LetterSpace' model offers an interface to explore the ABM parameters.
To run this model, first install the package into a new virtual environment as explained in the introduction.
Then, in a terminal change to the folder containing the correponding `run.py` file. For the LetterSpace model, this is `./scicom/letters/`.

In this folder execute the command `mesa runserver`. The output will give you a link to open in your browser, usually http://127.0.0.1:8521/. 

Open this link and explore.